package parser

import (
	"image"
	"io"
	"os"
)

func FromPath(path string) (*ImageParser, error) {
	if _, err := os.Stat(path); err == nil {
		// Open the io.Reader file
		file, err := os.Open(path)
		if err != nil {
			return nil, err
		}

		// Path io.Reader through to FromFile
		return FromFile(file)
	} else {
		return nil, err
	}
}

func FromFile(file io.Reader) (*ImageParser, error) {
	var err error

	// Try to decode the image we've been given
	image, _, err := image.Decode(file)
	if err != nil {
		return nil, err
	}

	// Parse our image
	imageParser := Parse(image)

	return imageParser, err
}
