package parser

import (
	"errors"
	"fmt"
	"image"
	"image/color"
	_ "image/gif"
	_ "image/jpeg"
	_ "image/png"
)

type ImageParser struct {
	// Input image, typically the output from an io.Reader
	Image image.Image

	// Our list of colours which we'll use to determine the mean
	colors map[color.RGBA]uint32

	// To check if we've already built our colours array
	isBuilt bool

	// The most frequent color
	highestColor color.RGBA

	// The frequency it occurs
	highestCount uint32
}

// Build our new and amazing ImageParser
func Parse(img image.Image) *ImageParser {
	return &ImageParser{
		Image:   img,
		colors:  make(map[color.RGBA]uint32),
		isBuilt: false,
	}
}

func (i *ImageParser) BuildColors() {
	// Loop through the pixels, row by row, column by column
	for x := 0; x < i.Image.Bounds().Dx(); x++ {
		for y := 0; y < i.Image.Bounds().Dy(); y++ {
			// Convert whatever the colour scheme is (RGBA, CMYK, etc) toRGBA
			pixelColor := color.RGBAModel.Convert(i.Image.At(x, y)).(color.RGBA)

			// Check if the colour isn't boring before we track it
			if !i.isBoring(pixelColor) {
				// Increment the count for this color
				i.colors[pixelColor]++
			}
		}
	}

	// Mark this image as built
	i.isBuilt = true
}

func (i *ImageParser) HighestColor() (color.RGBA, error) {
	// If we've not previously built the colors, do so now
	if !i.isBuilt {
		i.BuildColors()
	}

	err := i.buildHighestColor()

	return i.highestColor, err
}

func (i *ImageParser) HighestColorCount() (uint32, error) {
	// If we've not previously built the colors, do so now
	if !i.isBuilt {
		i.BuildColors()
	}

	err := i.buildHighestColor()

	return i.highestCount, err
}

func (i *ImageParser) ToHex() (string, error) {
	var err error
	highestColor, err := i.HighestColor()
	if err != nil {
		return "", err
	}

	return fmt.Sprintf("#%X%X%X", highestColor.R, highestColor.G, highestColor.B), err
}

func (i *ImageParser) buildHighestColor() error {
	i.highestCount = uint32(0)
	var err error

	// Loop through our pre-build colors to find the most common one
	for color, count := range i.colors {
		if count > i.highestCount {
			i.highestColor = color
			i.highestCount = count
		}
	}

	// If we don't end up with a high count, that's a problem
	if i.highestCount == 0 {
		err = errors.New("No colors were detected")
	}

	return err
}

func (i *ImageParser) isBoring(color color.RGBA) bool {
	// Basically check if the color is brighter than #eee
	return color.R > 238 && color.G > 238 && color.B > 238
}
