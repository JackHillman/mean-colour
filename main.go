package main

import (
	"flag"
	"fmt"

	"gitlab.com/JackHillman/mean-colour/parser"

	_ "image/jpeg"
	"log"
	"os"
)

var file = flag.String("file", "", "file to parse")

func main() {
	flag.Parse()
	image, err := parser.FromPath(*file)

	if err == nil {
		hex, err := image.ToHex()
		handle(err)

		count, err := image.HighestColorCount()
		handle(err)

		fmt.Printf("The mean colour is %s with %d occurences\n", hex, count)
	} else if os.IsNotExist(err) {
		log.Fatal(fmt.Sprintf("File %s does not exist\n", *file))
	}

	handle(err)
}

func handle(err error) {
	if err != nil {
		panic(err)
	}
}
